import sqlite3 as sq
from tkinter import *
import tkinter as tk
from tkinter import ttk

root = tk.Tk()
root.geometry('500x500+650+300')
root.resizable(False,False)
root.title('Login')
root.config(bg="#dbdbdb")

columns = ["object", "exam_date"]

root.rowconfigure(index=0, weight=1)
tree = ttk.Treeview(columns=columns, show="headings")
tree.grid(row=0, column=0, sticky="nsew")

tree.heading("object", text="Экзамен", anchor=W)
tree.heading("exam_date", text="Дата экзамена", anchor=W)

tree.column("#1", stretch=NO, width=250)
tree.column("#2", stretch=NO, width=250)

with sq.connect("Student.db") as conn:
    cur = conn.cursor()

for res in cur.execute("SELECT (object),(exam_date) FROM object"):
    tree.insert("", END, values=res)
    print(res)

scrollbar = ttk.Scrollbar(orient=VERTICAL, command=tree.yview)
tree.configure(yscroll=scrollbar.set)
scrollbar.grid(row=0, column=1, sticky="ns")

root.mainloop()