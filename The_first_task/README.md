# Интерфейс

При запуски программы открываеться окно:

![](https://imgur.com/O2Vo3cS.png)

Здесть пользователь может вести сумму в первой строчке он переведет рубли в доллары,
во второй строче доллары в рубли.

![](https://imgur.com/5p1sN5c.png)
