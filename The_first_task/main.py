import tkinter as tk

class FormView:
    def __init__(self):
        self.root = tk.Tk()
        self.root.geometry('500x300+650+300')
        self.root.resizable(False,False)
        self.root.title('Docker')
        self.root.config(bg="#dbdbdb")

        self.dollar = 0

        self.Label_entry_ruble = tk.Label(self.root, text='Введите рубль',font=("times new roman", 14), bg="#dbdbdb")
        self.Label_dollar = tk.Label(self.root, text=f'=', font=(14), bg="#dbdbdb")
        self.Label_entry_dollar = tk.Label(self.root, text='Введите доллар', font=("times new roman", 14), bg="#dbdbdb")
        self.Label_ruble = tk.Label(self.root, text=f'=', font=(14), bg="#dbdbdb")

        self.Label_entry_ruble.place(x=100, y=50)
        self.Label_dollar.place(x=230, y=77)
        self.Label_entry_dollar.place(x=100, y=150)
        self.Label_ruble.place(x=230, y=177)

        self.Entry_ruble = tk.Entry(self.root)
        self.Entry_dollar = tk.Entry(self.root)
        self.Entry_exit_ruble = tk.Entry(self.root)
        self.Entry_exit_dollar = tk.Entry(self.root)

        self.Entry_ruble.insert(0, "0")
        self.Entry_dollar.insert(0, "0")
        self.Entry_exit_ruble.insert(0, "0")
        self.Entry_exit_dollar.insert(0, "0")

        self.Entry_ruble.place(x=100, y=80)
        self.Entry_dollar.place(x=100, y=180)
        self.Entry_exit_ruble.place(x=250, y=80)
        self.Entry_exit_dollar.place(x=250, y=180)

        self.Button_ruble = tk.Button(self.root, text="Перевести в доллар", width=15, height=2,command=self.Ruble_dollar)
        self.Button_dollar = tk.Button(self.root, text="Перевести в рубли", width=15, height=2,command=self.Dollar_ruble)

        self.Button_ruble.place(x=100, y=110)
        self.Button_dollar.place(x=100, y=210)

        self.root.mainloop()

    def Ruble_dollar(self):
        Entry_ruble = float(self.Entry_ruble.get())
        # курс на 26.12.22 (1 = 68.68)
        self.ruble = Entry_ruble / 68.7

        self.Entry_exit_ruble = tk.Entry(self.root)
        self.Entry_exit_ruble.insert(0, f"{self.ruble}")
        self.Entry_exit_ruble.place(x=250, y=80)

    def Dollar_ruble(self):
        Entry_dollar = float(self.Entry_dollar.get())
        # курс на 26.12.22 (1 = 68.68)
        self.dollar = Entry_dollar * 68.7


        self.Entry_exit_dollar = tk.Entry(self.root)
        self.Entry_exit_dollar.insert(0, f"{self.dollar}")
        self.Entry_exit_dollar.place(x=250, y=180)




if __name__ == '__main__':
    FormView()

